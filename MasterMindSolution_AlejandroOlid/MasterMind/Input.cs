﻿using System;
namespace MasterMind
{
    public class Input
    {
        public static string InsertGuess(int intentos)
        {
            string guess;
            do
            {
                Console.Write("\n Tienes {0} intentos." +
                              "\n Escribe una combinación: ", intentos);
                guess = Console.ReadLine();
                if (guess != null && guess.Length != 4) Console.Write("\nError!! La combinación debe ser de 4 carácteres.\n");
            } while (guess is not {Length: 4});

            return guess;
        }

        public static string PlayAgain()
        {
            string option;
            do
            {
                Console.Write("\n ¿Quieres jugar otra vez? ");
                option = Console.ReadLine();
                if (option != null) option = option.ToLower();
            } while (option is not ("si" or "no"));
            return option;
        }
    }
}