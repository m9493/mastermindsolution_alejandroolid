﻿using System;

namespace MasterMind
{
    public static class Run
    {
        public static void MasterMind()
        {
            Console.Clear();
            var secret = MethodsToUse.NewStringSecret();
            var rightposition = 0;
            var intentos = 12; 
            Output.Head();
            for (; rightposition != 4 && intentos !=0 ; intentos--) //limitar los turnos
            {
                rightposition = 0;
                var wrongposition = 0;
                var guess = Input.InsertGuess(intentos);
                guess = guess.ToUpper();
                var adivinado = "";
                rightposition = MethodsToUse.CheckRightPosition(secret, guess, rightposition, ref adivinado);
                wrongposition = MethodsToUse.CheckWrongPosition(secret, guess, wrongposition, adivinado);
                Output.ShowStats(rightposition, wrongposition);
            }
            Output.WinOrLose(rightposition);
            var option = Input.PlayAgain();
            if (option == "si") MasterMind();
        }
    }
}