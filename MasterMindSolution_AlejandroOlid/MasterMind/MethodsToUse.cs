﻿using System;
namespace MasterMind
{
    public static class MethodsToUse
    {
                
        public static int CheckWrongPosition(string secret, string guess, int wrongposition, string adivinado)
        {
            for (var i = 0; i < 4; i++)
            {
                if (secret.Contains(guess[i]) && !adivinado.Contains(guess[i]))
                {
                    wrongposition++;
                }
            }
            return wrongposition;
        }

        public static int CheckRightPosition(string secret, string guess, int rightposition, ref string adivinado)
        {
            for (var i = 0; i < 4; i++)
            {
                if (secret[i] == guess[i])
                {
                    rightposition++;
                    adivinado += guess[i];
                }
            }
            return rightposition;
        }
        
        public static string NewStringSecret()
        {
            string[] characters = {"A", "B", "C", "D", "E", "F"}; //Combinación aleatoria
            var rnd = new Random();
            var secret = "";
            for (var j = 0; j < 4; j++)
            {
                var aux = rnd.Next(characters.Length);
                secret += characters[aux];
            }
            return secret;
        }
    }
}