/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: Controla un robot introduciendo comandos por teclado.
 * Controles del robot:       [Salir -> END]
                              ·Arriba -> Up
                              ·Abajo -> Down
                              ·Izquierda -> Left
                              ·Derecha -> Right
                              ·Mostrar posición -> Position
                              ·Acelerar -> SpeedUp 
                              ·Frenar -> SpeedDown
                              ·Mostrar velocidad -> Speed
                               */
using Exercises;
using NUnit.Framework;
namespace TestProject
{
    public class BasicRobotTest
    {
        [Test]
        public void BasicRobotUp()
        {
            double y = 0;
            const double v = 1;
            BasicRobot.Up(ref y, v);
        }
        [Test]
        public void BasicRobotDown()
        {
            double y = 0;
            const double v = 1;
            BasicRobot.Down(ref y, v);
        }
        [Test]
        public void BasicRobotRight()
        {
            double x = 0;
            const double v = 1;
            BasicRobot.Right(ref x, v);
        }
        [Test]
        public void BasicRobotLeft()
        {
            double x = 0;
            const double v = 1;
            BasicRobot.Left(ref x, v);
        }
        [Test]
        public void BasicRobotSpeedUp()
        {
            double v = 1;
            BasicRobot.SpeedUp(ref v);
        }
        [Test]
        public void BasicRobotSpeedDown()
        {
            double v = 1;
            BasicRobot.SpeedDown(ref v);
        }
        [Test]
        public void BasicRobotPosition()
        {
            double[] robotPosition = {1,1};
            BasicRobot.Position(robotPosition);
        }
        [Test]
        public void BasicRobotSpeed()
        {
            double v = 1;
            BasicRobot.Speed(v);
        }
    }
}