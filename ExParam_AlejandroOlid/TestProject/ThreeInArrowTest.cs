/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: 3 en raya. Introduce coordenadas [ABC-123] para rellenar las casillas del tablero.
 */
using Exercises;
using NUnit.Framework;
namespace TestProject
{
    public class ThreeInArrowTest
    {
        [Test]
        public void PaintTableTest()
        { 
            var table = new char[3,3];
            ThreeInArrow.PaintTable(ref table);
        }
        [Test]
        public void ShowTable()
        { 
            var table = new char[3,3];
            ThreeInArrow.ShowTable(table);
        }
        [Test]
        public void Winner()
        { 
            var table = new char[3,3];
            ThreeInArrow.Winner(table);
        }
        [Test]
        public void Coordinates()
        { 
            var table = new char[3,3];
            const string position = "A1";
            ThreeInArrow.Coordinates(table, position);
        }
        [Test]
        public void ValidCoordinate()
        { 
            const string position = "F2";
            ThreeInArrow.ValidCoordinate(position);
        }
        [Test]
        public void PutCoordinate()
        { 
            var table = new char[3,3];
            const string position = "A2";
            const char charPlayer = 'X';
            ThreeInArrow.PutCoordinate(ref table, position, charPlayer);
        }
        [Test]
        public void FullTable()
        { 
            var table = new char[3,3];
            ThreeInArrow.FullTable(table);
        }
    }
}