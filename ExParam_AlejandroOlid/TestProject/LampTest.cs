/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: ¿Cómo funciona una lampara? Introduce:
 */
using Exercises;
using NUnit.Framework;
namespace TestProject
{
    public class LampTest
    {
        [Test]
        public void SimulationTurnOn()
        {
            const string status = "turn on";
            Lamp.SimulationTest(status);
        }
        [Test]
        public void SimulationTurnOff()
        { 
            const string status = "turn off";
            Lamp.SimulationTest(status);
        }
        [Test]
        public void SimulationToggle()
        { 
            const string status = "toggle";
            Lamp.SimulationTest(status);
        }
        [Test]
        public void SimulationToLower()
        { 
            const string status = "toGgLE";
            Lamp.SimulationTest(status);
        }
    }
}