/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: Organiza las parcelas de un camping con los comandos Get In y Get Out.
 */
using Exercises;
using NUnit.Framework;
namespace TestProject
{
    public class CampSiteOrganizerTest
    {
        [Test]
        public void CampSiteRegisterReserve()
        {
            const int numPersons = 2;
            const string nameReserve = "Primo";
            var slots = 2;
            int[] persons = {4, 2, 6};
            string[] reserve = {"Juan", "Pepe", "Edu"};
            CampSiteOrganizer.RegisterReserveTest(ref persons,  ref reserve, ref slots, nameReserve, numPersons);
        }
        [Test]
        public void CampSiteDeleteReserve()
        {
            const string nameReserve = "Juan"; 
            var slots = 2;
            int[] persons = {4, 2, 6};
            string[] reserve = {"Juan", "Pepe", "Edu"};
            CampSiteOrganizer.DeleteReserveTest(ref persons,  ref reserve, ref slots, nameReserve);
        }
        [Test]
        public void CampSiteShowSlots()
        {
            const int slots = 2;
            int[] persons = {4, 2, 6};
            CampSiteOrganizer.ShowSlotsTest(persons, slots);
        }
    }
}