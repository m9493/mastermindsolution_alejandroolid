/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: Calcula el área y el perímetro de un triángulo rectángulo.
*/
using Exercises;
using NUnit.Framework;
namespace TestProject
{
    public class RightTriangleSizeTest
    {
        [Test]
        public void RightTriangleArea()
        {
            const double n = 10;
            const double m = 20;
            RightTriangleSize.Area(n, m);
        }
        [Test]
        public void RightTriangleGetSide3()
        {
            const double n = 10;
            const double m = 20;
            RightTriangleSize.GetSide3(n, m);
        }
        [Test]
        public void RightTrianglePerimeter()
        {
            const double n = 10;
            const double m = 20;
            var z = RightTriangleSize.GetSide3(n, m);
            RightTriangleSize.Perimeter(n, m, z);
        }
    }
}