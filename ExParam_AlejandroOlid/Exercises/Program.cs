﻿/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 */
namespace Exercises
{
    /// <summary>
    /// Main
    /// </summary>
    public static class Program
    {
        private static void Main()
        {
            Menu.RunMenu();
        }
    }
}