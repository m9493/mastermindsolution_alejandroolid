﻿/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: Controla un robot introduciendo comandos por teclado.
 * Controles del robot:       [Salir -> END]
                              ·Arriba -> Up
                              ·Abajo -> Down
                              ·Izquierda -> Left
                              ·Derecha -> Right
                              ·Mostrar posición -> Position
                              ·Acelerar -> SpeedUp 
                              ·Frenar -> SpeedDown
                              ·Mostrar velocidad -> Speed
                              */
using System;
using System.Globalization;
namespace Exercises
{
    /// <summary>
    /// Ejercicio BasicRobot
    /// </summary>
    public static class BasicRobot 
    {
        /// <summary>
        /// Método menu para iniciar el control del robot.
        /// </summary>
        public static void RobotMovement()
        {
            Console.WriteLine("\n Controles del robot: [Salir -> END]" + //Menu
                              "\nMovimiento:" +
                              "\t·Arriba -> Up" +
                              "\t·Abajo -> Down" +
                              "\t·Izquierda -> Left" +
                              "\t·Derecha -> Right" +
                              "\t·Mostrar posición -> Position" +
                              "\nVelocidad:" +
                              "\t\t·Acelerar -> SpeedUp" +
                              "\t\t·Frenar -> SpeedDown" +
                              "\t\t·Mostrar velocidad -> Speed");
            var ti = CultureInfo.CurrentCulture.TextInfo;
            var robotPosition = new double[] { 0, 0, 1}; //Valores
            string order;
            do // Bucle
            {
                Console.Write("\n=> ");
                order = Convert.ToString(Console.ReadLine());
                if (order != null)
                    switch (ti.ToLower(order))
                    {
                        case "up":
                            Up(ref robotPosition[1], robotPosition[2]);
                            break;
                        case "down":
                            Down(ref robotPosition[1], robotPosition[2]);
                            break;
                        case "right":
                            Right(ref robotPosition[0], robotPosition[2]);
                            break;
                        case "left":
                            Left(ref robotPosition[0], robotPosition[2]);
                            break;
                        case "position":
                            Position(robotPosition);
                            break;
                        case "speed":
                            Speed(robotPosition[2]);
                            break;
                        case "speedup":
                            SpeedUp(ref robotPosition[2]);
                            break;
                        case "speeddown":
                            SpeedDown(ref robotPosition[2]);
                            break;
                    }
            } while (order != null && ti.ToLower(order) != "end"); 
        }
        
        /// <summary>
        /// Método para aumentar la posición del robot.
        /// </summary>
        /// <param name="y">
        /// Parametro que hace referencia a la coordenada y.
        /// </param>
        /// <param name="v">
        /// Parametro que hace referencia a la velocidad, a mayor o menos velocidad, más o menos desplazamiento.
        /// </param>
        public static void Up(ref double y, double v)
        {
            y+=v;
        }
        
        /// <summary>
        /// Método para disminuir la posición del robot.
        /// </summary>
        /// <param name="y">
        /// Parametro que hace referencia a la coordenada y.
        /// </param>
        /// <param name="v">
        /// Parametro que hace referencia a la velocidad, a mayor o menos velocidad, más o menos desplazamiento.
        /// </param>
        public static void Down(ref double y,double v)
        {
            y-=v;
        }
        
        /// <summary>
        /// Método para aumentar la posición del robot.
        /// </summary>
        /// <param name="x">
        /// Parametro que hace referencia a la coordenada x.
        /// </param>
        /// <param name="v">
        /// Parametro que hace referencia a la velocidad, a mayor o menos velocidad, más o menos desplazamiento.
        /// </param>
        public static void Right(ref double x,double v)
        {
            x+=v;
        }
        
        /// <summary>
        /// Método para disminuir la posición del robot.
        /// </summary>
        /// <param name="x">
        /// Parametro que hace referencia a la coordenada x.
        /// </param>
        /// <param name="v">
        /// Parametro que hace referencia a la velocidad, a mayor o menos velocidad, más o menos desplazamiento.
        /// </param>
        public static void Left(ref double x,double v)
        {
            x-=v;
        }
        
        /// <summary>
        /// Método para aumentar la velocidad del robot
        /// </summary>
        /// <param name="v">
        /// Parametro que hace referencia a la velocidad, a mayor o menos velocidad, más o menos desplazamiento.
        /// </param>
        public static void SpeedUp(ref double v)
        {
            v+=0.5;
        }
        
        /// <summary>
        /// Método para disminuir la velocidad del robot
        /// </summary>
        /// <param name="v">
        /// Parametro que hace referencia a la velocidad, a mayor o menos velocidad, más o menos desplazamiento.
        /// </param>
        public static void SpeedDown(ref double v)
        {
            v-=0.5;
        }
        
        /// <summary>
        /// Método para mostrar por consola la posición del robot.
        /// </summary>
        /// <param name="robotPosition">
        /// Array de double donde se guardan las coordenadas del robot.
        /// </param>
        public static void Position(double[] robotPosition)
        {
            Console.WriteLine("\n Posición del robot: [ {0} | {1} ]",robotPosition[0], robotPosition[1]);
        }
        
        /// <summary>
        /// Método para mostrar por consola la velocidad del robot.
        /// </summary>
        /// <param name="v">
        /// Parametro que hace referencia a la velocidad, a mayor o menos velocidad, más o menos desplazamiento.
        /// </param>
        public static void Speed(double v)
        {
            Console.WriteLine("\n Velocidad del robot: {0}",v);
        }
    }
}