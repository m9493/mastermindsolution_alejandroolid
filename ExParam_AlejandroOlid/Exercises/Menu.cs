/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: Menú ejercicios parametrización
 */

using System;
namespace Exercises
{
    /// <summary>
    /// Menu
    /// </summary>
    public static class Menu
    {
        /// <summary>
        /// Método para iniciar el menu.
        /// </summary>
        public static void RunMenu()
        {
            var menu = true;
            while (menu)
            {
                Console.Write("\n INDICE DE EJERCICIOS:" +
                              "\n 01: RightTriangleSize" +
                              "\n 02: Lamp" + 
                              "\n 03: ThreeInArrow" + 
                              "\n 04: BasicRobot" + 
                              "\n 05: CampSiteOrganizer" + 
                              "\n 06: SquashCounter" + 
                              "\n Introduce 'salir' para finalizar el programa" + 
                              "\n Introduce un ejercicio: ");
                var option = Console.ReadLine();
                switch (option)
                {
                    default:
                        Console.WriteLine("Opción Incorrecta");
                        RunMenu();
                        break;
                    case "1":
                        Console.Clear();
                        RightTriangleSize.CalcTriangleSize();
                        break;
                    case "2":
                        Console.Clear();
                        Lamp.Simulation();
                        break;
                    case "3":
                        Console.Clear();
                        ThreeInArrow.LetsPlay();
                        break;
                    case "4":
                        Console.Clear();
                        BasicRobot.RobotMovement();
                        break;
                    case "5":
                        Console.Clear();
                        CampSiteOrganizer.Organizer();
                        break;
                    case "6":
                        Console.Clear();
                        SquashCounter.Counter();
                        break;
                    case "Salir":
                        menu = false;
                        break;
                }
                Console.Clear();
            }
        }
    }
}