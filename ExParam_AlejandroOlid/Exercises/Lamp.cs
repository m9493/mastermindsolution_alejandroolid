﻿/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: ¿Cómo funciona una lampara? Introduce:
 * Turn On para encender la luz.
 * Turn Off para apagar la luz.
 * Toggle para cambiar el estado actual.
 * Fin para salir.
 */
using System;
namespace Exercises
{
    /// <summary>
    /// Ejercicio CampSiteOrganizer
    /// </summary>
    public static class Lamp
    {
        /// <summary>
        /// Método introducir datos y simular una lampara. 
        /// </summary>
        public static void Simulation() 
        {
            var status = false;
            Console.WriteLine("Introduce:" +
                              "\n Turn On para encender la luz." +
                              "\n Turn Off para apagar la luz." +
                              "\n Toggle para cambiar el estado actual." +
                              "\n Introduce Fin para salir.");
            var exit = Console.ReadLine();
            if (exit != null)
            {
                exit = exit.ToLower();
                while (exit != "fin")
                {
                    switch (exit)
                    {
                        case "turn on":
                            status = TurnOn();
                            break;
                        case "turn off":
                            status = TurnOff();
                            break;
                        case "toggle":
                            status = Toggle(status);
                            break;
                        default:
                            Console.WriteLine("Intentalo de nuevo");
                            break;
                    }
                    Console.WriteLine("\t" + status);
                    exit = Console.ReadLine();
                }
            }
        }

        /// <summary>
        /// Método encender la lámpara. 
        /// </summary>
        private static bool TurnOn()
        {
            return true;
        }

        /// <summary>
        /// Método apagar la lámpara. 
        /// </summary>
        private static bool TurnOff()
        {
            return false;
        }

        /// <summary>
        /// Método invertir el estado actual de la lámpara. 
        /// </summary>
        /// <param name="status">
        /// Opción para modificar el estado de la lámpara.
        /// </param>
        private static bool Toggle(bool status)
        {
            return !status;
        }
        
        /// <summary>
        /// Método Test para simular una lámpara. 
        /// </summary>
        /// <param name="statusTest">
        /// Opción para modificar el estado de la lámpara.
        /// </param>
        public static void SimulationTest(string statusTest) // Método para Tests
        {
            var status = false;
            var exit = statusTest;
            if (exit != null)
            {
                exit = exit.ToLower();
                while (exit != "fin")
                {
                    switch (exit)
                    {
                        case "turn on":
                            status = TurnOn();
                            break;
                        case "turn off":
                            status = TurnOff();
                            break;
                        case "toggle":
                            status = Toggle(status);
                            break;
                        default:
                            Console.WriteLine("Intentalo de nuevo");
                            break;
                    }
                    Console.WriteLine("\t" + status);
                    exit = "fin";
                }
            }
        }
    }
}