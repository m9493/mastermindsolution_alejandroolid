﻿/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: Organiza las parcelas de un camping con los comandos Get In y Get Out.
*/
using System;
using System.Linq;

namespace Exercises
{
    /// <summary>
    /// Ejercicio CampSiteOrganizer
    /// </summary>
    public static class CampSiteOrganizer
    {
        /// <summary>
        /// Método para iniciar el programa de organizar e introducir datos.
        /// </summary>
        public static void Organizer()
        {
            var slots = 0;
            var persons = new int[] { };
            var reserve = new string[] { };
            string option;
            do
            {
                Console.WriteLine("\n ·Get In: Registrar una reserva" +
                                  "\n ·Get Out: Finalizar una reserva" +
                                  "\n ·Exit: Salir del programa\n");
                option = Console.ReadLine();
                if (option != null)
                {
                    option = option.ToLower();
                    switch (option)
                    {
                        case "get in":
                            RegisterReserve(ref persons, ref reserve, ref slots);
                            break;
                        case "get out":
                            DeleteReserve(ref persons,ref reserve, ref slots);
                            break;
                    }
                }
                ShowSlots(persons, slots);
            } while (option != "exit");
        }

        /// <summary>
        /// Método para mostrar los datos almacenados.
        /// </summary>
        /// <param name="persons">
        /// Array de números enteros que hace referencia a un número de personas.
        /// </param>
        /// <param name="slots">
        /// Número de parcelas ocupadas.
        /// </param>
        private static void ShowSlots(int[] persons, int slots)  //Muestra los datos del camping 
        {
            var sumPersons = persons.Sum();
            Console.WriteLine("\n\tParcelas: {0} " +
                              "\n\tPersonas: {1}", slots, sumPersons);
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// Método para eliminar una reserva.
        /// </summary>
        /// <param name="persons">
        /// Array de números enteros que hace referencia a un número de personas.
        /// </param>
        /// <param name="reserve">
        /// Array de string que hace referencia a un nombre de reserva.
        /// </param>
        /// <param name="slots">
        /// Número de parcelas ocupadas.
        /// </param>
        private static void DeleteReserve(ref int[] persons, ref string[] reserve, ref int slots)  
        {
            string nameReserve;
            do
            {
                Console.Write("Introduce el nombre de la reserva: ");
                nameReserve = Console.ReadLine();
            } while (nameReserve == null);
            for (var i = 0; i < reserve.Length; i++)
            {
                if (nameReserve == reserve[i])
                {
                    reserve[i] = reserve[reserve.Length - 1];
                    Array.Resize(ref reserve, reserve.Length - 1);
                    persons[i] = persons[persons.Length - 1];
                    Array.Resize(ref persons, persons.Length - 1);
                    slots--;
                }
            }
        }

        /// <summary>
        /// Método para introducir una reserva.
        /// </summary>
        /// <param name="persons">
        /// Array de números enteros que hace referencia a un número de personas.
        /// </param>
        /// <param name="reserve">
        /// Array de string que hace referencia a un nombre de reserva.
        /// </param>
        /// <param name="slots">
        /// Número de parcelas ocupadas.
        /// </param>
        private static void RegisterReserve(ref int[] persons,ref string[] reserve, ref int slots)  //Registrar reservas
        {
            int numPersons;
            string nameReserve;
            do
            {
                Console.Write("Introduce el número de personas: ");
                numPersons = Convert.ToInt32(Console.ReadLine());
                Console.Write("Introduce el nombre de la reserva: ");
                nameReserve = Console.ReadLine();
                if (nameReserve == null ) Console.WriteLine("No puedes introducir valores nulos.");
            } while (nameReserve == null);
            Array.Resize(ref persons, persons.Length + 1);
            persons[persons.Length - 1] = numPersons;
            Array.Resize(ref reserve, reserve.Length + 1);
            reserve[reserve.Length - 1] = nameReserve;
            slots++;
        }
        
        /// <summary>
        /// Método para eliminar una reserva.
        /// </summary>
        /// <param name="persons">
        /// Array de números enteros que hace referencia a un número de personas.
        /// </param>
        /// <param name="reserve">
        /// Array de string que hace referencia a un nombre de reserva.
        /// </param>
        /// <param name="slots">
        /// Número de parcelas ocupadas.
        /// </param>
        /// <param name="nameReserve">
        /// String que hace referencia a una reserva.
        /// </param>
        public static void DeleteReserveTest(ref int[] persons, ref string[] reserve, ref int slots, string nameReserve)   //Eliminar reserva TEST
        {
            for (var i = 0; i < reserve.Length; i++)
            {
                if (nameReserve == reserve[i])
                {
                    reserve[i] = reserve[reserve.Length - 1];
                    Array.Resize(ref reserve, reserve.Length - 1);
                    persons[i] = persons[persons.Length - 1];
                    Array.Resize(ref persons, persons.Length - 1);
                    slots--;
                }
            }
        }

        /// <summary>
        /// Método para eliminar una reserva.
        /// </summary>
        /// <param name="persons">
        /// Array de números enteros que hace referencia a un número de personas.
        /// </param>
        /// <param name="reserve">
        /// Array de string que hace referencia a un nombre de reserva.
        /// </param>
        /// <param name="slots">
        /// Número de parcelas ocupadas.
        /// </param>
        /// <param name="nameReserve">
        /// String que hace referencia a una reserva.
        /// </param>
        /// <param name="numPersons">
        /// Número de personas de una reserva.
        /// </param>
        public static void RegisterReserveTest(ref int[] persons,ref string[] reserve, ref int slots, string nameReserve, int numPersons)  //Registrar reservas TEST
        {
            Array.Resize(ref persons, persons.Length + 1);
            persons[persons.Length - 1] = numPersons;
            Array.Resize(ref reserve, reserve.Length + 1);
            reserve[reserve.Length - 1] = nameReserve;
            slots++;
        }
        
        
        /// <summary>
        /// Método para mostrar los datos almacenados.
        /// </summary>
        /// <param name="persons">
        /// Array de números enteros que hace referencia a un número de personas.
        /// </param>
        /// <param name="slots">
        /// Número de parcelas ocupadas.
        /// </param>
        public static void ShowSlotsTest(int[] persons, int slots)  //Muestra los datos del camping TEST 
        {
            var sumPersons = persons.Sum();
            Console.WriteLine("\n\tParcelas: {0} " +
                              "\n\tPersonas: {1}", slots, sumPersons);
        }
    }
}