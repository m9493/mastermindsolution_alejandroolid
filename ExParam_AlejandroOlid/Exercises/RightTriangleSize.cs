﻿/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: Calcula el área y el perímetro de un triángulo rectángulo.
*/
using System;
namespace Exercises
{
    /// <summary>
    /// Ejercicio RightTriangleSize
    /// </summary>
    public static class RightTriangleSize
    {
        /// <summary>
        /// Método para introducir número de triangulos y lados para calcular area y perímetro.
        /// </summary>
        public static void CalcTriangleSize()
        {
            Console.Write("\n Introduce el número de triángulos: ");
            var n = NumTriangles();
            for (var i = 0; i < n; i++)
            {
                Console.WriteLine("\n Lados del {0}º Triángulo:",i+1);
                var side1 = Convert.ToDouble(Console.ReadLine());
                var side2 = Convert.ToDouble(Console.ReadLine());
                var side3 = GetSide3(side1, side2);
                var result1 = Area(side1, side2);
                var result2 = Perimeter(side1, side2, side3);
                Console.WriteLine("Un triangulo de {0} x {1} tiene {2} de área y {3} de perímetro.",side1,side2,result1,result2);
            }
            Console.ReadKey();
        }

        /// <summary>
        /// Método para pedir número de triangulos 
        /// </summary>
        /// <returns>
        /// Número de triangulos
        /// </returns>
        private static int NumTriangles()
        {
            var n = Convert.ToInt32(Console.ReadLine());
            return n;
        }
        
        /// <summary>
        /// Método calcular el area de un triángulo
        /// </summary>
        /// <param name="side1">
        /// lado de un triángulo
        /// </param>
        /// <param name="side2">
        /// lado de un triángulo
        /// </param>
        /// <returns>
        /// Area del triángulo
        /// </returns>
        public static double Area(double side1, double side2)
        {
            var result = (side1*side2)/2;
            return result;
        }
        
        /// <summary>
        /// Método calcular el perímetro de un triángulo
        /// </summary>
        /// <param name="side1">
        /// lado de un triángulo
        /// </param>
        /// <param name="side2">
        /// lado de un triángulo
        /// </param>
        /// <param name="side3">
        /// lado de un triángulo
        /// </param>
        /// <returns>
        /// Perímetro de un triángulo
        /// </returns>
        public static double Perimeter(double side1, double side2,double side3)
        {
            var result = side1+side2+side3;
            return result;
        }

        /// <summary>
        /// Método calcular el tercer lado de un triángulo
        /// </summary>
        /// <param name="side1">
        /// lado de un triángulo
        /// </param>
        /// <param name="side2">
        /// lado de un triángulo
        /// </param>
        /// <returns>
        /// lado de un triángulo
        /// </returns>
        public static double GetSide3(double side1, double side2)
        {
            var side3 = Math.Sqrt((side1 * side1) + (side2 * side2));
            return side3;
        }
    }
}