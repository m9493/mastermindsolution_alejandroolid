﻿/* Alumno: Alejandro Olid
 * Fecha: 07/02/2022
 * Descripción: 3 en raya. Introduce coordenadas [ABC-123] para rellenar las casillas del tablero.
 */
using System;
namespace Exercises
{
    /// <summary>
    /// Ejercicio ThreeInArrow
    /// </summary>
    public static class ThreeInArrow
    {
        /// <summary>
        /// Método para iniciar el juego
        /// </summary>
        public static void LetsPlay()
        {
            var table = new char[3,3]; //Crear tablero
            PaintTable(ref table);
            Console.Clear();
            var player = true;
            char charPlayer;
            Console.WriteLine("Jugador 1, introduce tu nombre: ");
            var namePlayer1 = Console.ReadLine();
            Console.WriteLine("Jugador 2, introduce tu nombre: ");
            var namePlayer2 = Console.ReadLine();
            while (!Winner(table) && FullTable(table)) //Bucle 2 Jugadores 
            {
                Console.Clear();
                ShowTable(table);
                string position;
                switch (player)
                {
                    case true:
                        do
                        {
                            charPlayer = 'X';
                            Console.Write(" {0}: ",namePlayer1);
                            position = Console.ReadLine();
                            if (!ValidCoordinate(position)) Console.WriteLine("Coordenada erronea. Intentalo de nuevo.");
                            if (Coordinates(table, position)) Console.WriteLine("La coordenada ya está ocupada. Intentalo de nuevo.");
                        } while (!ValidCoordinate(position) || Coordinates(table, position));
                        break;
                    case false:
                        do
                        {
                            charPlayer = 'O';
                            Console.Write(" {0}: ",namePlayer2);
                            position = Console.ReadLine();
                            if (!ValidCoordinate(position)) Console.WriteLine("Coordenada erronea. Intentalo de nuevo.");
                            if (Coordinates(table, position)) Console.WriteLine("La coordenada ya está ocupada. Intentalo de nuevo.");
                        } while (!ValidCoordinate(position) || Coordinates(table, position));
                        break;
                }
                PutCoordinate(ref table, position, charPlayer);
                player = !player;
            }
            Console.Clear(); //Fin del juego
            ShowTable(table);
            var ganador = player switch
            {
                true => namePlayer1,
                false => namePlayer2
            };
            if (Winner(table)) Console.WriteLine(" FELICIDADES HAS GANADO " + ganador + "!!!" +
                                                 "\n Presiona cualquier tecla para volver al menu.");
            if (!FullTable(table)) Console.WriteLine(" No hay más espacio en el tablero." +
                                                    "\n Presiona cualquier tecla para volver al menu.");
            Console.ReadKey();
        }

        /// <summary>
        /// Método para rellenar el array de carácteres.
        /// </summary>
        /// <param name="table">
        /// Array de carácteres que simula un tablero de 3 en raya.
        /// </param>
        public static void PaintTable(ref char[,] table)
        {
            for (var i = 0; i < table.GetLength(0); i++)
            {
                for (var j = 0; j < table.GetLength(1); j++)
                {
                    table[i, j] = '◌';
                }
            }
        }

        /// <summary>
        /// Método para mostrar el array de carácteres.
        /// </summary>
        /// <param name="table">
        /// Array de carácteres que simula un tablero de 3 en raya.
        /// </param>
        public static void ShowTable(char[,] table)
        {
            var tableToShow = 
                "\n" +
                "\t     1   2   3  \n" + 
                "\t   ┌───┬───┬───┐\n" + 
                $"\tA  │ {table[0,0]} │ {table[0,1]} │ {table[0,2]} │\n"+
                "\t   ├───┼───┼───┤\n" + 
                $"\tB  │ {table[1,0]} │ {table[1,1]} │ {table[1,2]} │\n"+ 
                "\t   ├───┼───┼───┤\n" +
                $"\tC  │ {table[2,0]} │ {table[2,1]} │ {table[2,2]} │\n"+
                "\t   └───┴───┴───┘\n";
            Console.WriteLine(tableToShow);
        } 

        /// <summary>
        /// Método para comprobar si hay ganador.
        /// </summary>
        /// <param name="table">
        /// Array de carácteres que simula un tablero de 3 en raya.
        /// </param>
        /// <returns>
        /// Parámetro boolean para comprobar si hay ganador.
        /// </returns>
        public static bool Winner(char[,] table)
        {
            if (table[0, 0] == table[0, 1] && table[0, 1] == table[0, 2] && table[0, 0] != '◌') return true;
            if (table[1, 0] == table[1, 1] && table[1, 1] == table[1, 2] && table[1, 0] != '◌') return true;
            if (table[2, 0] == table[2, 1] && table[2, 1] == table[2, 2] && table[2, 0] != '◌') return true;
            if (table[0, 0] == table[1, 0] && table[1, 0] == table[2, 0] && table[0, 0] != '◌') return true;
            if (table[0, 1] == table[1, 1] && table[1, 1] == table[2, 1] && table[0, 1] != '◌') return true;
            if (table[0, 2] == table[1, 2] && table[1, 2] == table[2, 2] && table[0, 2] != '◌') return true;
            if (table[0, 0] == table[1, 1] && table[1, 1] == table[2, 2] && table[0, 0] != '◌') return true;
            if (table[0, 2] == table[1, 1] && table[1, 1] == table[2, 0] && table[0, 2] != '◌') return true;
            return false;
        } 
        
        /// <summary>
        /// Método para controlar las coordenadas introducidas.
        /// </summary>
        /// <param name="table">
        /// Array de carácteres que simula un tablero de 3 en raya.
        /// </param>
        /// <param name="x">
        /// Coordenada "x" 
        /// </param>
        /// <param name="y">
        /// Coordenada "y" 
        /// </param>
        /// <returns>
        /// boolean para controlar si la casilla está ocupada.
        /// </returns>

        public static bool Exceptions(char[,] table, int x, int y)
        {
            if (x < 0 || x > 2) throw new ArgumentException("Introduce 0, 1 o 2","x");
            if (y < 0 || y > 2) throw new ArgumentException("Introduce 0, 1 o 2","y");
            return table[x,y] != '◌';
        }  

        /// <summary>
        /// Método para vincular coordenadas con posiciones de un vector
        /// </summary>
        /// <param name="table">
        /// Array de carácteres que simula un tablero de 3 en raya.
        /// </param>
        /// <param name="position">
        /// string introducido por consola vinculado a una posición.
        /// </param>
        /// <returns>
        /// boolean para controlar si la casilla cumple las excepciones.
        /// </returns>
        public static bool Coordinates(char[,] table, string position)
        {
            position = position.ToUpper();
            switch (position)
            {
                case "A1":
                    return Exceptions(table, 0, 0);
                case "A2":
                    return Exceptions(table, 0, 1);
                case "A3":
                    return Exceptions(table, 0, 2);
                case "B1":
                    return Exceptions(table, 1, 0);
                case "B2":
                    return Exceptions(table, 1, 1);
                case "B3":
                    return Exceptions(table, 1, 2);
                case "C1":
                    return Exceptions(table, 2, 0);
                case "C2":
                    return Exceptions(table, 2, 1);
                case "C3":
                    return Exceptions(table, 2, 2);
                default:
                    return false;
            }
        } 

        /// <summary>
        /// Método para comprobar coordenadas
        /// </summary>
        /// <param name="position">
        /// string introducido por consola vinculado a una posición.
        /// </param>
        /// <returns>
        /// boolean para controlar si es una coordenada válida.
        /// </returns>
        public static bool ValidCoordinate(string position)
        {
            position = position.ToUpper();
            switch (position)
            {
                case "A1":
                case "A2": 
                case "A3": 
                case "B1":
                case "B2": 
                case "B3":
                case "C1": 
                case "C2":
                case "C3":
                    return true;
                default:
                    return false;
            }
        } 

        
        /// <summary>
        /// Método para introducir char nuevos en el tablero
        /// </summary>
        /// <param name="table">
        /// Array de carácteres que simula un tablero de 3 en raya.
        /// </param>
        /// <param name="position">
        /// string introducido por consola vinculado a una posición.
        /// </param>
        /// <param name="charPlayer">
        /// Char único para cada jugador.
        /// </param>
        public static void PutCoordinate(ref char[,] table, string position, char charPlayer)
        {
            position = position.ToUpper();
            switch (position)
            {
                case "A1":
                    table [0, 0] = charPlayer;
                    break;
                case "A2":
                    table [0, 1] = charPlayer;
                    break;
                case "A3":
                    table [0, 2] = charPlayer;
                    break;
                case "B1":
                    table [1, 0] = charPlayer;
                    break;
                case "B2":
                    table [1, 1] = charPlayer;
                    break;
                case "B3":
                    table [1, 2] = charPlayer;
                    break;
                case "C1":
                    table [2, 0] = charPlayer;
                    break;
                case "C2":
                    table [2, 1] = charPlayer;
                    break;
                case "C3":
                    table [2, 2] = charPlayer;
                    break;
            }
        }   

        /// <summary>
        /// Método para comprobar si el tablero está lleno.
        /// </summary>
        /// <param name="table">
        /// Array de carácteres que simula un tablero de 3 en raya.
        /// </param>
        /// <returns>
        /// Boolean para comprobar si el tablero está lleno.
        /// </returns>
        public static bool FullTable(char[,] table)
        {
            var full = false;
            for (var i = 0; i < table.GetLength(0); i++)
            {
                for (var j = 0; j < table.GetLength(1); j++)
                {
                    if (table[i, j] == '◌') full = true;
                }
            }
            return full;
        } 
    }
}