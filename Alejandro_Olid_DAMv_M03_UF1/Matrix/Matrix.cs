﻿using System;

namespace Matrix
{
    class Matrix
    {
        static void SimpleBattleshipResult() //Introduce un valor de la matriz para comprobar si hay barcos en esta posición.
        {
            char[,] ships =
            {   {'X' ,'X' ,'0', '0', '0', '0', 'X'},
                {'0' ,'0' ,'X', '0', '0', '0', 'X'},
                {'0' ,'0' ,'0', '0', '0', '0', '0'},
                {'0' ,'X' ,'X', 'X', '0', '0', 'X'},
                {'0' ,'0' ,'0', '0', 'X', '0', '0'},
                {'0' ,'0' ,'0', '0', 'X', '0', '0'},
                {'X' ,'0' ,'0', '0', '0', '0', '0'}
            };
            Console.Write("Introduce la fila: ");
            int x = Convert.ToInt32(Console.ReadLine()!);
            Console.Write("Introduce la columna: ");
            int y = Convert.ToInt32(Console.ReadLine()!);
            Console.WriteLine("");
            if (ships[x, y] == 'X') Console.WriteLine("Tocado!");
            else Console.WriteLine("Agua!");
            Console.WriteLine("");
            Console.WriteLine("CAMPO DE BATALLA:");
            for (int i = 0; i < ships.GetLength(0); i++)
            {
                Console.Write("| ");
                for (int j = 0; j < ships.GetLength(1); j++)
                {
                    Console.Write("{0} ",ships[i,j]);
                }
                Console.WriteLine("|");
            }
        }

        static void MatrixElementSum() //Suma de una matriz
        {
            int[,] matrix = {{2, 5, 1, 6}, {23, 52, 14, 36}, {23, 75, 81, 64}};
            Console.WriteLine("Matriz:");
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                { 
                    Console.Write("{0}\t",matrix[i,j]);
                }
                Console.WriteLine("");
            }
            int result = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    result += matrix[i, j];
                }
            }
            Console.WriteLine("");
            Console.WriteLine("La suma de la matriz es: {0}", result);
        }

        static void MatrixBoxesOpenedCounter()
        {
            int[,] box = new int[4,4];
            Console.WriteLine("Introduce -1 en 'Valor X' para salir del programa.");
            Console.WriteLine("Introduce dos valores entre 0 y 3 para acceder a la caja de seguridad: ");
            Console.Write("Valor X: ");
            int x = Convert.ToInt32(Console.ReadLine()!);
            while (x != -1)
            { 
                    Console.Write("Valor Y: ");
                    int y = Convert.ToInt32(Console.ReadLine()!);
                    box[x, y] += 1;
                    Console.Write("Valor X: ");
                    x = Convert.ToInt32(Console.ReadLine()!);
            }
            Console.Write("");
            Console.WriteLine("Registro diario de las cajas de seguridad:");
            for (int i = 0; i < box.GetLength(0); i++)
            {
                Console.Write("[");
                for (int j = 0; j < box.GetLength(1); j++)
                {
                    if (j == box.GetLength(1) - 1) Console.Write("{0}", box[i, j]);
                    else Console.Write("{0}, ",box[i, j]);
                }
                Console.Write("]");
            }
        }

        static void MatrixIsThereADiv13() //Si algún número de la matriz es divisible por 13 imprimirá true por pantalla, en caso negativo, false.
        {
            int[,] matrix = {{2, 5, 1, 6}, {23, 52, 14, 36}, {23, 75, 81, 62}};
            bool div13 = false;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] % 13 == 0)
                    {
                        div13 = true;
                        break;
                    }
                }
            }
            Console.WriteLine("Matriz:");
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write("{0}\t",matrix[i,j]);
                }
                Console.WriteLine("");
            }
            Console.WriteLine("");
            Console.WriteLine("¿Algún número de la matriz es divisible por 13? {0}",div13);
        }

        static void HighestMountainOnMap()
        {
            double[,] map =
            {
                {1.5, 1.6, 1.8, 1.7, 1.6},
                {1.5, 2.6, 2.8, 2.7, 1.6}, 
                {1.5, 4.6, 4.4, 4.9, 1.6},
                {2.5, 1.6, 3.8, 7.7, 3.6}, 
                {1.5, 2.6, 3.8, 2.7, 1.6}
            };
            double result = 0;
            int x = 0;
            int y = 0;
            Console.WriteLine("Mapa:");
            for (int i = 0; i < map.GetLength(0); i++)
            {
                Console.Write("[");
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] > result)
                    {
                        result = map[i, j];
                        x = i;
                        y = j;
                    }
                    if (j == map.GetLength(1) - 1) Console.Write("{0}", map[i, j]);
                    else Console.Write("{0}\t",map[i,j]);
                }
                Console.WriteLine("]");
            }
            Console.WriteLine("");
            Console.WriteLine("La cima más alta se encuentra en [{0},{1}], con una altura de {2} metros.",x,y,result);
        }
        
        static void HighestMountainScaleChange()
        {
        double[,] map =
        {
            {1.5, 1.6, 1.8, 1.7, 1.6},
            {1.5, 2.6, 2.8, 2.7, 1.6}, 
            {1.5, 4.6, 4.4, 4.9, 1.6},
            {2.5, 1.6, 3.8, 7.7, 3.6}, 
            {1.5, 2.6, 3.8, 2.7, 1.6}
        };
        double result = 0;
        int x = 0;
        int y = 0;
        Console.WriteLine("Map:");
        for (int i = 0; i < map.GetLength(0); i++)
        {
            Console.Write("[");
            for (int j = 0; j < map.GetLength(1); j++)
            {
                map[i, j] = Math.Round(map[i, j]*3.2808, 2); //2 Decimales
                if (map[i, j] > result)
                {
                    result = map[i, j];
                    x = i;
                    y = j;
                }
                if (j == map.GetLength(1) - 1) Console.Write("{0}", map[i, j]);
                else Console.Write("{0}\t",map[i,j]);
            }
            Console.WriteLine("]");
        }
        Console.WriteLine("");
        Console.WriteLine("The highest peak is at [{0}, {1}], with a height of {2} feets.",x,y,result);
        }

        static void MatrixSum()
        {
            Console.Write("Nº Filas de 1º Matriz: ");
            int x1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nº Columnas de 1º Matriz: ");
            int y1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nº Filas de 2º Matriz: ");
            int x2 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nº Columnas de 2º Matriz: ");
            int y2 = Convert.ToInt32(Console.ReadLine());
            
            if (x1 != x2 || y1 != y2) Console.WriteLine("Para sumar matrices deben tener el mismo número de filas y columnas!!!");
            else
            {
                int[,] matrix1 = new int[x1,y1];
                int[,] matrix2 = new int[x1,y1];
                int[,] result = new int[x1,y1];
                Console.WriteLine("1º Matriz:");
                for (int i = 0; i < matrix1.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix1.GetLength(1); j++)
                    {
                        matrix1[i, j] = Convert.ToInt32(Console.ReadLine());
                    }
                }
                Console.WriteLine("2º Matriz:");
                for (int i = 0; i < matrix2.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix1.GetLength(1); j++)
                    {
                            matrix2[i, j] = Convert.ToInt32(Console.ReadLine());
                    }
                }
                Console.WriteLine("Suma de las matrices:");
                for (int i = 0; i < result.GetLength(0); i++)
                {
                    Console.Write("[");
                    for (int j = 0; j < result.GetLength(1); j++)
                    {
                        result[i, j] = matrix1[i, j] + matrix2[i, j];
                        if (j == result.GetLength(1) - 1) Console.Write("{0}", result[i, j]);
                        else Console.Write("{0} ",result[i,j]);
                    }
                    Console.WriteLine("]");
                }
            }
        }

        static void RookMoves()
        {
            Console.Write("Introduce un número [1-8] para elegir una fila: ");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduce una letra [A-I] para elegir una columna: ");
            string yString = Convert.ToString(Console.ReadLine());
            yString = yString?.ToUpper();
            int yInt = 0;
            switch (yString)
            {
                case "A":
                    yInt = 0;
                    break;
                case "B":
                    yInt = 1;
                    break;
                case "C":
                    yInt = 2;
                    break;
                case "D":
                    yInt = 3;
                    break;
                case "F":
                    yInt = 4;
                    break;
                case "G":
                    yInt = 5;
                    break;
                case "H":
                    yInt = 6;
                    break;
                case "I":
                    yInt = 7;
                    break;
            } 
            string[,] table = new String[8,8];
            Console.WriteLine();
            Console.WriteLine("Tablero de ajedrez:");
            for (int i = 0; i < table.GetLength(0); i++)
            {
                Console.Write("|");
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    if ( i == x-1 && j == yInt)
                        table[i, j] = "♜";
                    else if ( i == x-1 )
                        table[i, j] = "♖";
                    else if ( j == yInt ) 
                        table[i, j] = "♖";
                    else
                        table[i, j] = "X";
                    Console.Write(" {0}",table[i,j]);
                }
                Console.WriteLine(" |");
            }
        }

        static void MatrixSimetric()
        {
            Console.Write("Introduce el número de filas: ");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduce el número de columnas: ");
            int y = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Matriz:");
            int[,] matrix = new int[x,y];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            int[,] matrixTrans = new int[y,x];
            for (int i = 0; i < matrixTrans.GetLength(0); i++)
            {
                for (int j = 0; j < matrixTrans.GetLength(1); j++)
                {
                    matrixTrans[i, j] = matrix[j,i];
                }
            }
            Console.WriteLine("");
            Console.WriteLine("Matriz Original:");
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                Console.Write("|");
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(" {0}",matrix[i,j]);
                }
                Console.WriteLine(" |");
            }
            Console.WriteLine("");
            Console.WriteLine("Matriz Transpuesta:");
            for (int i = 0; i < matrixTrans.GetLength(0); i++)
            {
                Console.Write("|");
                for (int j = 0; j < matrixTrans.GetLength(1); j++)
                {
                    Console.Write(" {0}",matrixTrans[i,j]);
                }
                Console.WriteLine(" |");
            }
            Console.WriteLine("");
            bool symmetric = true;
            Console.WriteLine("Si la matriz es igual a su traspuesta, entonces será una matriz simétrica.");
            if (x == y)
            {
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    if (symmetric == false) break;
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        if (matrix[i, j] != matrixTrans[i, j])
                        {
                            symmetric = false;
                            break;
                        }
                    }
                }
            }
            else symmetric = false;
            Console.WriteLine("¿Esta matriz es simétrica? {0}",symmetric);
        }

        static void Menu()
        {
            Console.WriteLine("INDICE EJERCICIOS:");
            Console.WriteLine(" 01: SimpleBattleshipResult");
            Console.WriteLine(" 02: MatrixElementSum");
            Console.WriteLine(" 03: MatrixBoxesOpenedCounter");
            Console.WriteLine(" 04: MatrixIsThereADiv13");
            Console.WriteLine(" 05: HighestMountainOnMap");
            Console.WriteLine(" 06: HighestMountainScaleChange");
            Console.WriteLine(" 07: MatrixSum");
            Console.WriteLine(" 08: RookMoves");
            Console.WriteLine(" 09: MatrixSimetric");

            Console.Write("Introduce el número del ejercicio que quieras ejecutar: ");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("");
            switch (n)
            {
                case 1:
                    SimpleBattleshipResult();
                    break;
                case 2:
                    MatrixElementSum();
                    break;
                case 3:
                    MatrixBoxesOpenedCounter();
                    break;
                case 4:
                    MatrixIsThereADiv13();
                    break;
                case 5:
                    HighestMountainOnMap();
                    break;
                case 6:
                    HighestMountainScaleChange();
                    break;
                case 7:
                    MatrixSum();
                    break;
                case 8:
                    RookMoves();
                    break;
                case 9:
                    MatrixSimetric();
                    break;
            }
        }

        static void Main()
        {
            Menu();
        }
    }
}