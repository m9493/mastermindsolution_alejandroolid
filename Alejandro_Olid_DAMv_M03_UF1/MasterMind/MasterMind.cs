﻿// Autor: Alejandro Olid
using System;

namespace MasterMind
{
    class MasterMind
    {
        static void Main()
        {
            Console.WriteLine("Versiones disponibles: 0 / 1");
            Console.Write("¿A qué versión de MasterMind quieres jugar? ");
            int n = Convert.ToInt32(Console.ReadLine());
            switch (n)
            {
                case 0:
                    MasterMindV0();
                    break;
                case 1:
                    MasterMindV1();
                    break;
            }
        }

        static void MasterMindV0()
        {
            string secret = "ABCD";
            int rightposition = 0;
            int intentos = 12;
            Console.WriteLine("  __  __               _____   _______   ______   _____          __  __   _____   _   _   _____         __      __   ___  ");
            Console.WriteLine(@" |  \/  |     /\      / ____| |__   __| |  ____| |  __ \        |  \/  | |_   _| | \ | | |  __ \        \ \    / /  / _ \ ");
            Console.WriteLine(@" | \  / |    /  \    | (___      | |    | |__    | |__) |       | \  / |   | |   |  \| | | |  | |        \ \  / /  | | | |");
            Console.WriteLine(@" | |\/| |   / /\ \    \___ \     | |    |  __|   |  _  /        | |\/| |   | |   | . ` | | |  | |         \ \/ /   | | | |");
            Console.WriteLine(@" | |  | |  / ____ \   ____) |    | |    | |____  | | \ \        | |  | |  _| |_  | |\  | | |__| |          \  /    | |_| |");
            Console.WriteLine(@" |_|  |_| /_/    \_\ |_____/     |_|    |______| |_|  \_\       |_|  |_| |_____| |_| \_| |_____/            \/      \___/ ");
            Console.WriteLine("");
            Console.WriteLine("Adivina la combinación aleatoria de 4 letras:" +
                              "- Posibles letras: A B C D E F" +
                              "- RightPosition: Indica que hay una letra correcta y en su posición." +
                              "- WrongPosition: Indica que hay una letra correcta, pero su posición en erronea.");
            Console.WriteLine("");

            for (; rightposition != 4; intentos--) //limitar los turnos
            {
                rightposition = 0;
                int wrongposition = 0;
                Console.WriteLine("Tienes {0} intentos.", intentos);
                Console.Write("Escribe una combinación: ");
                string guess = Console.ReadLine() ?? "";

                if (guess.Length != 4)
                {
                    Console.Write("Escribe una combinación de 4 carácteres: ");
                    guess = Console.ReadLine();
                }

                string adivinado = "";

                for (int i = 0; i < 4; i++)
                {
                    if (guess != null && secret[i] == guess[i])
                    {
                        rightposition++;
                        adivinado += guess[i];
                    }
                }

                for (int i = 0; i < 4; i++)
                {
                    if (guess != null && secret.Contains(guess[i]) && !adivinado.Contains(guess[i]))
                    {
                        wrongposition++;
                    }
                }

                Console.WriteLine("RightPosition: " + rightposition);
                Console.WriteLine("WrongPosition: " + wrongposition);
                Console.WriteLine("");

                if (intentos == 1)
                {
                    break;
                }
            }

            if (rightposition == 4) // Dibujitos
            {
                Console.WriteLine("¡ FELICIDADES, HAS ACERTADO !");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░█▄");
                Console.WriteLine("░▄▄▄▄▄▄░░░░░░░░░░░░░▄▄▄▄▄░░█▄");
                Console.WriteLine("░▀▀▀▀▀███▄░░░░░░░▄███▀▀▀▀░░░█▄");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
                Console.WriteLine("░▄▀▀▀▀▀▄░░░░░░░░░░▄▀▀▀▀▀▄░░░░█");
                Console.WriteLine("█▄████▄░▀▄░░░░░░▄█░▄████▄▀▄░░█▄");
                Console.WriteLine("████▀▀██░▀▄░░░░▄▀▄██▀█▄▄█░█▄░░█");
                Console.WriteLine("██▀██████░█░░░░█░████▀█▀██░█░░█");
                Console.WriteLine("████▀▄▀█▀░█░░░░█░█████▄██▀▄▀░░█");
                Console.WriteLine("███████▀░█░░░░░░█░█████▀░▄▀░░░█");
                Console.WriteLine("░▀▄▄▄▄▄▀▀░░░░░░░░▀▀▄▄▄▄▀▀░░░░░█");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
                Console.WriteLine("░░▓▓▓▓▓▓▓░░░░░░░░░░▓▓▓▓▓▓▓░░░░█");
                Console.WriteLine("░░░▓▓▓▓▓░░░░░░░░░░░░▓▓▓▓▓░░░░░█");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█▀");
                Console.WriteLine("░░░░░░░░░▄▄███████▄▄░░░░░░░░░█");
                Console.WriteLine("░░░░░░░░█████████████░░░░░░░█▀");
                Console.WriteLine("░░░░░░░░░▀█████████▀░░░░░░░█▀");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░█▀");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░█▀");
            }
            else
            {
                Console.WriteLine("Eres malisimo, ponte a estudiar...");
                Console.WriteLine("░░░░░░░░░░░░▄▄▄█▀▀▀▀▀▀▀▀█▄▄▄░░░░░░░░░░░░");
                Console.WriteLine("░░░░░░░░▄▄█▀▀░░░░░░░░░░░░░░▀▀█▄▄░░░░░░░░");
                Console.WriteLine("░░░░░░▄█▀░░░░▄▄▄▄▄▄▄░░░░░░░░░░░▀█▄░░░░░░");
                Console.WriteLine("░░░░▄█▀░░░▄██▄▄▄▄▄▄▄██▄░░░░▄█▀▀▀▀██▄░░░░");
                Console.WriteLine("░░░█▀░░░░█▀▀▀░░▄██░░▄▄█░░░██▀▀▀███▄██░░░");
                Console.WriteLine("░░█░░░░░░▀█▀▀▀▀▀▀▀▀▀██▀░░░▀█▀▀▀▀███▄▄█░░");
                Console.WriteLine("░█░░░░░░░░░▀▀█▄▄██▀▀░░░░░░░░▀▄▄▄░░░▄▄▀█░");
                Console.WriteLine("█▀░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▀▀▀▀▀░░▀█");
                Console.WriteLine("█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▄░░░░█");
                Console.WriteLine("█░░░░░░░░░░░░░░░░░░░░░░░░▄▄▄▄▄██░░▀█░░░█");
                Console.WriteLine("█░░░░░░░░░░░░░░█░░░▄▄▄█▀▀▀░░░▄█▀░░░░░░░█");
                Console.WriteLine("█░░░░░░░░░░░░░░░░░░▀░░░░░░░░█▀░░░░░░░░░█");
                Console.WriteLine("█▄░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▄█");
                Console.WriteLine("░█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█░");
                Console.WriteLine("░░█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█░░");
                Console.WriteLine("░░░█▄░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▄█░░░");
                Console.WriteLine("░░░░▀█▄░░░░░░░░░░░░░░░░░░░░░░░░░░▄█▀░░░░");
                Console.WriteLine("░░░░░░▀█▄░░░░░░░░░░░░░░░░░░░░░░▄█▀░░░░░░");
            }
        }

        static void MasterMindV1()
        {
            string[] characters = {"A", "B", "C", "D", "E", "F"}; //Combinación aleatoria
            Random rnd = new Random();
            string secret = "";

            for (int j = 0; j < 4; j++)
            {
                int aux = +(rnd.Next(characters.Length));
                secret += characters[aux];
            }

            int rightposition = 0;
            int intentos = 12;
            Console.WriteLine(".___  ___.      ___           _______..___________. _______ .______            .___  ___.  __  .__   __.  _______        ____    ____  __ ");
            Console.WriteLine(@"|   \/   |     /   \         /       ||           ||   ____||   _  \           |   \/   | |  | |  \ |  | |       \       \   \  /   / /_ |");
            Console.WriteLine(@"|  \  /  |    /  ^  \       |   (----``---|  |----`|  |__   |  |_)  |          |  \  /  | |  | |   \|  | |  .--.  |       \   \/   /   | |");
            Console.WriteLine(@"|  |\/|  |   /  /_\  \       \   \        |  |     |   __|  |      /           |  |\/|  | |  | |  . `  | |  |  |  |        \      /    | |");
            Console.WriteLine(@"|  |  |  |  /  _____  \  .----)   |       |  |     |  |____ |  |\  \----.      |  |  |  | |  | |  |\   | |  '--'  |         \    /     | |");
            Console.WriteLine(@"|__|  |__| /__/     \__\ |_______/        |__|     |_______|| _| `._____|      |__|  |__| |__| |__| \__| |_______/           \__/      |_|");
            Console.WriteLine("");
            Console.WriteLine("Adivina la combinación aleatoria de 4 letras:");
            Console.WriteLine("- Posibles letras: A B C D E F");
            Console.WriteLine("- RightPosition: Indica que hay una letra correcta y en su posición.");
            Console.WriteLine("- WrongPosition: Indica que hay una letra correcta, pero su posición es erronea.");
            Console.WriteLine("");
            
            for (; rightposition != 4; intentos--) //limitar los turnos
            {
                rightposition = 0;
                int wrongposition = 0;
                Console.WriteLine("Tienes {0} intentos.", intentos);
                Console.Write("Escribe una combinación: ");
                string guess = Console.ReadLine() ?? "";
                
                guess = guess.ToUpper();  //Condiciones
                if (guess.Length != 4)
                {
                    Console.Write("Escribe una combinación de 4 carácteres: ");
                    guess = Console.ReadLine();
                }

                string adivinado = "";

                for (int i = 0; i < 4; i++)
                {
                    if (guess != null && secret[i] == guess[i])
                    {
                        rightposition++;
                        adivinado += guess[i];
                    }
                }

                for (int i = 0; i < 4; i++) 
                {
                    if (guess != null && secret.Contains(guess[i]) && !adivinado.Contains(guess[i]))
                    {
                        wrongposition++;
                    }
                }

                Console.WriteLine("RightPosition: " + rightposition);
                Console.WriteLine("WrongPosition: " + wrongposition);
                Console.WriteLine("");

                if (intentos == 1)
                {
                    break;
                }
            }

            if (rightposition == 4) // Dibujitos
            {
                Console.WriteLine("¡ FELICIDADES, HAS ACERTADO !");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░█▄");
                Console.WriteLine("░▄▄▄▄▄▄░░░░░░░░░░░░░▄▄▄▄▄░░█▄");
                Console.WriteLine("░▀▀▀▀▀███▄░░░░░░░▄███▀▀▀▀░░░█▄");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
                Console.WriteLine("░▄▀▀▀▀▀▄░░░░░░░░░░▄▀▀▀▀▀▄░░░░█");
                Console.WriteLine("█▄████▄░▀▄░░░░░░▄█░▄████▄▀▄░░█▄");
                Console.WriteLine("████▀▀██░▀▄░░░░▄▀▄██▀█▄▄█░█▄░░█");
                Console.WriteLine("██▀██████░█░░░░█░████▀█▀██░█░░█");
                Console.WriteLine("████▀▄▀█▀░█░░░░█░█████▄██▀▄▀░░█");
                Console.WriteLine("███████▀░█░░░░░░█░█████▀░▄▀░░░█");
                Console.WriteLine("░▀▄▄▄▄▄▀▀░░░░░░░░▀▀▄▄▄▄▀▀░░░░░█");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
                Console.WriteLine("░░▓▓▓▓▓▓▓░░░░░░░░░░▓▓▓▓▓▓▓░░░░█");
                Console.WriteLine("░░░▓▓▓▓▓░░░░░░░░░░░░▓▓▓▓▓░░░░░█");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█▀");
                Console.WriteLine("░░░░░░░░░▄▄███████▄▄░░░░░░░░░█");
                Console.WriteLine("░░░░░░░░█████████████░░░░░░░█▀");
                Console.WriteLine("░░░░░░░░░▀█████████▀░░░░░░░█▀");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░░█▀");
                Console.WriteLine("░░░░░░░░░░░░░░░░░░░░░░░░░█▀");
            }
            else
            {
                Console.WriteLine("Eres malisimo, ponte a estudiar...");
                Console.WriteLine("░░░░░░░░░░░░▄▄▄█▀▀▀▀▀▀▀▀█▄▄▄░░░░░░░░░░░░");
                Console.WriteLine("░░░░░░░░▄▄█▀▀░░░░░░░░░░░░░░▀▀█▄▄░░░░░░░░");
                Console.WriteLine("░░░░░░▄█▀░░░░▄▄▄▄▄▄▄░░░░░░░░░░░▀█▄░░░░░░");
                Console.WriteLine("░░░░▄█▀░░░▄██▄▄▄▄▄▄▄██▄░░░░▄█▀▀▀▀██▄░░░░");
                Console.WriteLine("░░░█▀░░░░█▀▀▀░░▄██░░▄▄█░░░██▀▀▀███▄██░░░");
                Console.WriteLine("░░█░░░░░░▀█▀▀▀▀▀▀▀▀▀██▀░░░▀█▀▀▀▀███▄▄█░░");
                Console.WriteLine("░█░░░░░░░░░▀▀█▄▄██▀▀░░░░░░░░▀▄▄▄░░░▄▄▀█░");
                Console.WriteLine("█▀░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▀▀▀▀▀░░▀█");
                Console.WriteLine("█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▄░░░░█");
                Console.WriteLine("█░░░░░░░░░░░░░░░░░░░░░░░░▄▄▄▄▄██░░▀█░░░█");
                Console.WriteLine("█░░░░░░░░░░░░░░█░░░▄▄▄█▀▀▀░░░▄█▀░░░░░░░█");
                Console.WriteLine("█░░░░░░░░░░░░░░░░░░▀░░░░░░░░█▀░░░░░░░░░█");
                Console.WriteLine("█▄░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▄█");
                Console.WriteLine("░█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█░");
                Console.WriteLine("░░█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█░░");
                Console.WriteLine("░░░█▄░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▄█░░░");
                Console.WriteLine("░░░░▀█▄░░░░░░░░░░░░░░░░░░░░░░░░░░▄█▀░░░░");
                Console.WriteLine("░░░░░░▀█▄░░░░░░░░░░░░░░░░░░░░░░▄█▀░░░░░░");
            }
        }
    }
}