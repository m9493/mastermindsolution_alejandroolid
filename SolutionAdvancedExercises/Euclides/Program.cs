﻿/*

Autor: Alejandro Olid
Ejercicio: Euclides
Descripción: Muestra el valor factorial del valor introducido

*/
/*
using System;

namespace Euclides
{
    internal static class Euclides
    {
        private static void Main()
        {
            Console.WriteLine("Introduce dos valores: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());

            for (int r; b > 0;)
            {
                r = a % b;

                if (r != 0)
                {
                    a = b;
                    b = r;
                }
                else
                {
                    Console.WriteLine("El máximo común divisor es: " + b);
                    b = r;
                }
            }
        }
    }
}
*/