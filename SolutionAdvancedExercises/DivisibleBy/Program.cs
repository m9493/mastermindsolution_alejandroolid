﻿/*

Autor: Alejandro Olid
Ejercicio: DivisibleBy
Descripción: Muestra por pantallas los valores divisibles entre 3 (entre un rango de valores).

*/
/*
using System;

namespace DivisibleBy
{
    internal static class DivisibleBy
    {
        private static void Main()
        {
            Console.WriteLine("Introduce un rango de valores: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());

            if (a > b)
            {
                (a, b) = (b, a);
            }
            
            Console.Write("Divisibles entre 3: ");
            
            for (int n = 0; n <= b; n = n+3)
            {
                if (n >= a && n <= b)
                {
                    Console.Write(n + " ");
                }
            }
        }
    }
}
*/