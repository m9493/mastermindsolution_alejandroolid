﻿/*

Autor: Alejandro Olid
Ejercicio: PiramidCenter
Descripción: Imprime una piramide centrada por pantalla con la altura deseada.

*/
/*
using System;

namespace PiramidCenter
{
    internal static class PiramidCenter
    {
        private static void Main()
        {
            Console.Write("Introduce la altura de la piramide: ");
            var altura = Convert.ToInt32(Console.ReadLine());

            for (var a = 0; a < altura; a++)
            {

                for (var b = 0; b < altura - a; b++)
                {
                    Console.Write(" ");
                }

                for (var b = altura - 1; b < altura + a; b++)
                {
                    Console.Write("# ");
                }

                Console.WriteLine();
            }
        }
    }
}
*/