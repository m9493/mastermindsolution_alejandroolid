﻿/*

Autor: Alejandro Olid
Ejercicio: ToBinnary
Descripción: Traduce un número del sistema decimal al sistema binario

*/
/*
using System;

namespace ToBinnary
{
     internal static class ToBinnary
    {
        private static void Main()
        {
            Console.Write("Introduce un número entero: ");
            int n = Convert.ToInt32(Console.ReadLine());
            string binario = "";

            for (int resto; n != 0;)
            {
                resto = n % 2;
                string r = Convert.ToString(resto);
                binario = r + binario;
                n = n / 2;
            }
            Console.Write("Número binario: " + binario);
        }
    }
}
*/
