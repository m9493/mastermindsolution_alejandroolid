﻿/*

Autor: Alejandro Olid
Ejercicio: Factorial
Descripción: Muestra el valor factorial del valor introducido

*/

using System;

namespace Factorial
{
    internal static class Factorial
    {
        private static void Main()
        {
            Console.Write ("Introduce un número: ");
            int v = Convert.ToInt32 (Console.ReadLine());
            int f = 1;
            
            for (; v != 0 ; v--)
            {
                f = f * v;
            } 
            
            Console.WriteLine ("Valor factorial: " + f);
        }
    }
}
